{::options parse_block_html="true" /}

# Mermaid
```mermaid
graph TD
A[Click example]
A-->B
click B "http://www.github.com" "This is a link"
```

# Mark down examples
<details>
{::options parse_block_html="true" /}
  <summary>
      collapsible
  </summary>
  
  Use br for new lines.<br>
  You can insert code snippets.
  ```bash
  git pull
  ```
  
  ```bash
  git pull
  ```
</details>